import React from 'react';
import orderBy from 'lodash/orderBy';


export default class SortableTable extends React.Component {
  data = [
    {name: "abc", num: 20, date: "2008-11-24", note: "This",},
    {name: "dba", num: 8, date: "2004-03-01", note: "column",},
    {name: "ecd", num: 6, date: "1979-07-23", note: "cannot",},
    {name: "cut", num: 4.2, date: "1492-12-08", note: "be",},
    {name: "001", num: 0, date: "1601-08-13", note: "sorted.",},
    {name: "eof", num: 2, date: "1979-07-23", note: "Never.",},
  ];

  state = {
    sortByColumn: 'name',
    sortDirection: 'asc',
  };

  setSorting = column => {
    this.setState(prevState => {
      return ({
        sortByColumn: column,
        sortDirection: column === prevState.sortByColumn
          ? (prevState.sortDirection === 'desc' ? 'asc' : 'desc')
          : 'asc',
      });
    });
  };

  getSortedTable = () => {
    const { sortByColumn, sortDirection } = this.state;
    return orderBy(this.data, sortByColumn, sortDirection);
  };

  getSortingArrow = column => {
    const { sortByColumn, sortDirection } = this.state;
    if (sortByColumn !== column) {
      return '';
    }
    return sortDirection === 'asc' ? '↑' : '↓';
  };

  render() {
    return (
      <table>
        <thead>
        <tr>
          <th onClick={() => this.setSorting('name')}>{this.getSortingArrow('name')} Alphabetic</th>
          <th onClick={() => this.setSorting('num')}>{this.getSortingArrow('num')} Numeric</th>
          <th onClick={() => this.setSorting('date')}>{this.getSortingArrow('date')} Date</th>
          <th>Unsortable</th>
        </tr>
        </thead>
        <tbody>
        {this.getSortedTable().map(({name, num, date, note}) =>
          <tr key={name}>
            <td>{name}</td>
            <td>{num}</td>
            <td>{date}</td>
            <td>{note}</td>
          </tr>
        )}
        </tbody>
      </table>
    )
  }
}