import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import SortableTable from './SortableTable';

const App = () =>
  (
    <>
      <GlobalStyle/>
      <Root>
        <View>
          <SortableTable />
        </View>
      </Root>
    </>
  );

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
      "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    height: 100%;
  }
  
  html {
    width: 100%;
    height: 100%;
  }
  
  #react-app {
    height: 100%;
  }
`;


const Root = styled.div`
  width: 100%;
  display: flex;
  min-height: 100%;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const View = styled.div`
  flex: 1;
  background-color: #fff;
  width: 100%;
  
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;


export default App;
